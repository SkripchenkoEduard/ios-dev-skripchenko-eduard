//
//  AppDelegate.h
//  Block
//
//  Created by skripa on 4/4/16.
//  Copyright © 2016 Skripchenko Eduard. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

