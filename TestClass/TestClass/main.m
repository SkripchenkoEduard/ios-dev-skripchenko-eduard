//
//  main.m
//  TestClass
//
//  Created by skripa on 3/24/16.
//  Copyright © 2016 Skripchenko Eduard. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
