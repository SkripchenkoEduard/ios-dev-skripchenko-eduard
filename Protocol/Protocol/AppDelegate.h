//
//  AppDelegate.h
//  Protocol
//
//  Created by skripa on 3/28/16.
//  Copyright © 2016 Skripchenko Eduard. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

