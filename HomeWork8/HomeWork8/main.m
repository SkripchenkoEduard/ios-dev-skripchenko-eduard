//
//  main.m
//  SingleView
//
//  Created by Oleksandr Kosylov on 4/11/16.
//  Copyright © 2016 Oleksandr Kosylov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Task.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        TaskManager *manager = [TaskManager new];
        manager.maxConcurrentOperationCount = 2;
        manager.queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        
        
        for (NSInteger i=0; i<20; ++i) {
            Task *task = [[LogTask alloc] initWithText:@"Hi" success:^{
                //NSLog(@"success");
                
            } failure:^(NSError *error) {
                //NSLog(@"error:%@", error);
            }];
            
            [manager addTask:task];
        }
        
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
