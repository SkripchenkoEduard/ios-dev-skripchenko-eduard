//
//  Task.h
//  SingleView
//
//  Created by Oleksandr Kosylov on 4/11/16.
//  Copyright © 2016 Oleksandr Kosylov. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^VoidBlock)();
typedef void (^ErrorBlock)(NSError *error);

@interface Task : NSObject

@property BOOL finished;
@property BOOL executing;

@property BOOL cancelled;

- (void)execute;
- (void)stop;

@end

@interface TaskManager : NSObject

@property dispatch_queue_t queue;

////Возвращает общедоступную глобальную одновременную очередь с заданным уровнем приоритета.
//dispatch_queue_t dispatch_get_global_queue(long priority, unsigned long flags);
//Аргументы:
//priority	Приоритет очереди для ее извлечения. Возможные значения:
//DISPATCH_QUEUE_PRIORITY_HIGH
//DISPATCH_QUEUE_PRIORITY_DEFAULT
//DISPATCH_QUEUE_PRIORITY_LOW
//DISPATCH_QUEUE_PRIORITY_BACKGROUND

@property NSInteger maxConcurrentOperationCount;
@property NSInteger operationCount;

- (void)addTask:(Task *)task;

- (void)cancelAllOperations;

@end



@interface LogTask : Task

- (instancetype)initWithText:(NSString *)text success:(VoidBlock)success failure:(ErrorBlock)failre;

@end

