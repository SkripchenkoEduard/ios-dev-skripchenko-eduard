//
//  Task.m
//  SingleView
//
//  Created by Oleksandr Kosylov on 4/11/16.
//  Copyright © 2016 Oleksandr Kosylov. All rights reserved.
//

#import "Task.h"

@implementation Task

- (void)execute {
    
}

- (void)stop {
    self.cancelled = YES;
}

@end


@interface LogTask ()

@property NSString *text;

@property (copy) VoidBlock success;
@property (copy) ErrorBlock failure;

@end

@implementation LogTask

- (instancetype)initWithText:(NSString *)text success:(VoidBlock)success failure:(ErrorBlock)failure {
    self = [super init];
    
    if (self) {
        _text = text;
        _success = success;
        _failure = failure;
    }
    
    return self;
}

- (void)execute {
    self.executing = YES;
    
    if (self.cancelled) {
        self.failure([NSError errorWithDomain:@"" code:-1 userInfo:nil]);
        
        self.finished = YES;
        self.executing = NO;
        
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (weakSelf.cancelled) {
            weakSelf.failure([NSError errorWithDomain:@"" code:-1 userInfo:nil]);
            
            weakSelf.finished = YES;
            weakSelf.executing = NO;
            
            return;
        }
        
        NSLog(@"%@", weakSelf.text);
        
        weakSelf.success();
        
        weakSelf.finished = YES;
        weakSelf.executing = NO;
    });
}

@end

@interface TaskManager ()

@property NSMutableArray *tasks;

@end

@implementation TaskManager

- (instancetype)init {
    self = [super init];
    if (self) {
        _tasks = [NSMutableArray array];
    }
    return self;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    
    NSLog(@"currentOperationCount:%ld", self.operationCount);
    
    Task *task = object;
    
    if ([keyPath isEqualToString:@"finished"]) {
        if (task.finished) {
            --self.operationCount;
            
            [task removeObserver:self forKeyPath:@"finished"];
            [task removeObserver:self forKeyPath:@"executing"];
            
            [self.tasks removeObject:task];
            
            NSInteger idx = [self.tasks indexOfObjectPassingTest:^BOOL(Task  * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                return !obj.executing && !obj.finished;
            }];
            
            if (idx != NSNotFound) {
                Task *taskToExecute = self.tasks[idx];
                
                [self executeTask:taskToExecute];
            }
        }
        
    } else if ([keyPath isEqualToString:@"executing"]) {
        if (task.executing) {
            ++self.operationCount;
        }
    }
}

- (void)executeTask:(Task *)task {
    dispatch_sync(self.queue, ^{
        if (self.operationCount < self.maxConcurrentOperationCount) {
//            dispatch_async(self.queue, ^{
                [task execute];
//            });
        }
    });
}

- (void)addTask:(Task *)task {
    [task addObserver:self forKeyPath:@"finished" options:NSKeyValueObservingOptionNew context:nil];
    [task addObserver:self forKeyPath:@"executing" options:NSKeyValueObservingOptionNew context:nil];
    
    [self.tasks addObject:task];
    
    [self executeTask:task];
}

- (void)cancelAllOperations {
    [self.tasks makeObjectsPerformSelector:@selector(stop)];
}

@end

